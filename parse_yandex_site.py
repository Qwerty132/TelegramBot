import requests
from time import sleep
from bs4 import BeautifulSoup


def get_english_city_name(city):
    city = city.lower()
    if 'a' <= city[0] <= 'z' or 'A' <= city[0] <= 'Z':
        return city
    if city == '�����-���������' or city == '�����':
        return 'saint-petersburg'
    #    city = '����� ' + city
    url = "https://translate.yandex.net/api/v1.5/tr.json/translate?" \
          "key=trnsl.1.1.20180519T184430Z.ff5afd211d447b9e.d7eb0fc8a508092c07558d6274288b69b01b5f80" \
          "&text={text}&lang=ru-en&".format(text=city)
    sleep(5)
    req = requests.get(url=url)
    return req.json()['text'][0].split(' ')[-1].lower()


class Film:
    poster_url = ''
    name = ''
    genre = ''
    argument = ''
    rating = 0

    def __init__(self):
        pass


def comparator(film):
    return -film.rating


def parse_cinema_city(city):
    city = get_english_city_name(city)
    url = 'https://afisha.yandex.ru/{city}/selections/cinema-today'.format(city=city)
    ret_list = []
    req = requests.get(url, headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1)'
                      ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
                       )
    print(req.text)
    html_full_text = BeautifulSoup(req.text, 'html.parser').findAll(class_='events-list__list')[0]
    for html_text_one_film in BeautifulSoup(str(html_full_text), 'html.parser').findAll(class_='event'):
        current_film = Film()

        html_photo = BeautifulSoup(str(html_text_one_film), 'html.parser').findAll(class_='event__image')[0]
        html_rating = BeautifulSoup(str(html_photo), 'html.parser').findAll(class_='event-rating__value')
        html_photo = BeautifulSoup(str(html_photo), 'html.parser').findAll(class_='microdata')

        html_text = BeautifulSoup(str(html_text_one_film), 'html.parser').findAll(class_='event__inner')[0]
        html_genre = BeautifulSoup(str(html_text), 'html.parser').findAll(class_='event__formats')
        html_name = BeautifulSoup(str(html_text), 'html.parser').findAll(class_='event__name')
        html_argument = BeautifulSoup(str(html_text), 'html.parser').findAll(class_='event__argument')

        current_film.poster_url = str(html_photo[0])[33:125]
        current_film.rating = float(html_rating[0].get_text()) if len(html_rating) == 1 else 0
        current_film.genre = html_genre[0].get_text() if len(html_genre) == 1 else ''
        current_film.name = html_name[0].get_text() if len(html_name) == 1 else ''
        current_film.argument = html_argument[0].get_text() if len(html_argument) == 1 else ''
        ret_list.append(current_film)
    return ret_list


if __name__ == '__main__':
    parse_cinema_city('������')
