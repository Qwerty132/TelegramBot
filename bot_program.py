from telegram.ext import Updater, CommandHandler, MessageHandler
from telegram import ReplyKeyboardMarkup

import parse_yandex_site

city = dict()
order_films = dict()
position = dict()


# ��������� ������
def add_keyboard(bot, update, text):
    custom_keyboard = [['���������', '����������']]
    chat_id = update.message.chat_id
    reply_markup = ReplyKeyboardMarkup(custom_keyboard)
    bot.send_message(chat_id=chat_id, text=text, reply_markup=reply_markup)


# �������� ���� ������ ����
def get_list_cinema(bot, chat_id):
    bot.sendChatAction(chat_id=chat_id, action='upload_document')
    city.setdefault(chat_id, '������')
    order_films[chat_id] = sorted(parse_yandex_site.parse_cinema_city(city[chat_id]), key=parse_yandex_site.comparator)
    position[chat_id] = len(order_films[chat_id])


# ��������� ���������
def start(bot, update):
    chat_id = update.message.chat_id
    first_name = update.effective_user.first_name
    text = '������������, {name}. ' \
           '���� �� ����������� ����� �������, ����������� /help ��� ������������'.format(name=first_name)
    add_keyboard(bot, update, text)


# ��������
def help(bot, update):
    chat_id = update.message.chat_id
    city.setdefault(chat_id, '������')
    text = '���� ��� ������� ��� ������� ���� � ��������� ������. ' \
           '����� ������� �����, �������� "����� <��������������� �����>". ������� ����� - {city}. ' \
           '����� ������������� ����� ��������, ����������� /next � /prev, ' \
           '�������� ��������� � ���������� �� ��������, ��������������. ' \
           '������ ���������� ������ � https://afisha.yandex.ru, ' \
           '������� ������ ����� ����������� ������ ��������. ' \
           '���� ����� ������������ �����������, ��� �� �� ������������ ������, ' \
           '�� ������ ����������� �������� ��� �� ��������.'.format(city=city[chat_id])
    bot.send_message(chat_id=chat_id, text=text)


# ������ �������
def send_bot_film(bot, update):
    chat_id = update.message.chat_id
    bot.sendChatAction(action='upload_photo', chat_id=chat_id)
    film = order_films[chat_id][position[chat_id]]
    text = film.name
    text += '\n' + film.argument
    text += '\n����: ' + film.genre
    text += ('\n�������: ' + str(film.rating) if film.rating != 0 else ' ')
    bot.sendPhoto(chat_id=chat_id, photo=film.poster_url)
    bot.send_message(chat_id=chat_id, text=text)


# ����� ����������
def get_prev(bot, update):
    chat_id = update.message.chat_id
    if chat_id not in order_films:
        get_list_cinema(bot, chat_id)
    if position[chat_id] == 0:
        position[chat_id] = len(order_films[chat_id]) - 1
    else:
        position[chat_id] -= 1
    send_bot_film(bot, update)


# ����� ���������
def get_next(bot, update):
    chat_id = update.message.chat_id
    if chat_id not in order_films:
        get_list_cinema(bot, chat_id)
    if position[chat_id] >= len(order_films[chat_id]) - 1:
        position[chat_id] = 0
    else:
        position[chat_id] += 1
    send_bot_film(bot, update)


# ����� ������
def choose_city(bot, update):
    raw_text = update.message.text.split()
    if len(raw_text) != 2:
        incorrect_data(bot, update, '������������ ����� ������. ���������� ��� ���.')
    chat_id = update.message.chat_id
    city[chat_id] = raw_text[1]
    get_list_cinema(bot, chat_id)
    text = '������� ����� �� {city}'.format(city=city[chat_id])
    add_keyboard(bot, update, text)


# ������ ������������ ������
def incorrect_data(bot, update,
                   text='�� ������� ������. ����������� /help ��� ��������� �������������.'):
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=text)


def main():
    mytoken = '582042135:AAH_NTuPdQuQiPxESZ54coBblvyRQZMG5mg'
    updater = Updater(token=mytoken)
    # ��������� ��� ��������
    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CommandHandler('help', help))
    updater.dispatcher.add_handler(CommandHandler('next', get_next))
    updater.dispatcher.add_handler(CommandHandler('prev', get_prev))
    updater.dispatcher.add_handler(MessageHandler(lambda text: text.text == '���������', get_next))
    updater.dispatcher.add_handler(MessageHandler(lambda text: text.text == '����������', get_prev))
    updater.dispatcher.add_handler(MessageHandler(
        lambda text: text.text.split()[0] in ['�����', '�����'], choose_city))
    updater.dispatcher.add_handler(MessageHandler(lambda value: True, incorrect_data))
    # ������
    updater.start_polling(clean=True)
    updater.idle()


if __name__ == '__main__':
    main()
